FROM python:3.7.3-slim-stretch
FROM ubuntu:19.10
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y apt-utils python python3 python-pip python3-pip
#RUN pip install -U pip
RUN pip3 install -U pip
RUN pip3 install numpy pandas pandas-gbq gsutil